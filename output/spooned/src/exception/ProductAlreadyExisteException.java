package exception;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class ProductAlreadyExisteException extends Exception {
    private static Logger LOGGER = Logger.getLogger(ProductAlreadyExisteException.class.getName());

    private FileHandler fh;

    private IOException e1638976231001;

    public ProductAlreadyExisteException() {
        super();
        // TODO Auto-generated constructor stub
        try { fh = new FileHandler("ProductAlreadyExisteException.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public ProductAlreadyExisteException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
        try { fh = new FileHandler("ProductAlreadyExisteException.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }
}
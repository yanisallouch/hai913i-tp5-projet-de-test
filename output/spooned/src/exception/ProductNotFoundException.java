package exception;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class ProductNotFoundException extends Exception {
    private static Logger LOGGER = Logger.getLogger(ProductNotFoundException.class.getName());

    private FileHandler fh;

    private IOException e1638976231001;

    public ProductNotFoundException() {
        super();
        // TODO Auto-generated constructor stub
        try { fh = new FileHandler("ProductNotFoundException.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public ProductNotFoundException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
        try { fh = new FileHandler("ProductNotFoundException.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }
}
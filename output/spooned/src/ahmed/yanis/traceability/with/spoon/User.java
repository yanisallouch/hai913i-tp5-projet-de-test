package ahmed.yanis.traceability.with.spoon;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class User {
    private static Logger LOGGER = Logger.getLogger(User.class.getName());

    private FileHandler fh;

    private IOException e1638976231001;

    private String ID;

    private String name;

    private String age;

    private String email;

    private String password;

    public User() {
        super();
        try { fh = new FileHandler("User.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public User(String iD, String name, String age, String email, String password) {
        super();
        ID = iD;
        this.name = name;
        this.age = age;
        this.email = email;
        this.password = password;
        try { fh = new FileHandler("User.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public String getID() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return ID;
    }

    public void setID(String iD) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +iD.toString());
        ID = iD;
    }

    public String getName() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return name;
    }

    public void setName(String name) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +name.toString());
        this.name = name;
    }

    public String getAge() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return age;
    }

    public void setAge(String age) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +age.toString());
        this.age = age;
    }

    public String getEmail() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return email;
    }

    public void setEmail(String email) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +email.toString());
        this.email = email;
    }

    public String getPassword() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return password;
    }

    public void setPassword(String password) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +password.toString());
        this.password = password;
    }

    @Override
    public String toString() {
        LOGGER.info("no parameters given");
        return ((((((((("User [ID=" + ID) + ", name=") + name) + ", age=") + age) + ", email=") + email) + ", password=") + password) + "]";
    }
}
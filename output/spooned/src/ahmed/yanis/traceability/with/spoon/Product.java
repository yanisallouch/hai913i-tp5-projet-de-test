package ahmed.yanis.traceability.with.spoon;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class Product {
    private static Logger LOGGER = Logger.getLogger(Product.class.getName());

    private FileHandler fh;

    private IOException e1638976231000;

    private String ID;

    private String name;

    private String price;

    private String expirationDate;

    public Product(String iD, String name, String price, String expirationString) {
        super();
        ID = iD;
        this.name = name;
        this.price = price;
        this.expirationDate = expirationString;
        try { fh = new FileHandler("Product.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public Product() {
        super();
        try { fh = new FileHandler("Product.log",true); LOGGER.addHandler(fh); } catch (SecurityException e) { LOGGER.severe("Impossible to open FileHandler"); }catch (IOException e) { LOGGER.severe("Impossible to open FileHandler"); };
    }

    public String getID() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return ID;
    }

    public void setID(String iD) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +iD.toString());
        ID = iD;
    }

    public String getName() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return name;
    }

    public void setName(String name) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +name.toString());
        this.name = name;
    }

    public String getPrice() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return price;
    }

    public void setPrice(String price) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +price.toString());
        this.price = price;
    }

    public String getExpirationString() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        return expirationDate;
    }

    public void setExpirationString(String expirationString) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +expirationString.toString());
        this.expirationDate = expirationString;
    }

    @Override
    public String toString() {
        LOGGER.info("no parameters given");
        return ((((((("Product [ID=" + ID) + ", name=") + name) + ", price=") + price) + ", expirationString=") + expirationDate) + "]";
    }
}
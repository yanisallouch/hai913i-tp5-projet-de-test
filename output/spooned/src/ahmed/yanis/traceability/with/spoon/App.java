package ahmed.yanis.traceability.with.spoon;
import exception.ProductAlreadyExisteException;
import exception.ProductNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
/**
 *
 *
 * @author ALLOUCH Yanis
 * @author KACI Ahmed
 */
public class App {
    private static Logger LOGGER = Logger.getLogger(App.class.getName());

    private FileHandler fh;

    private IOException e1638976230999;

    static User user = new User();

    static Repository repo = new Repository();

    static Scanner sc;

    public static final String QUIT = "-1";

    public static void main(String[] args) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +args.toString());
        System.out.println("A simple CLI application!");
        sc = new Scanner(System.in);
        String userInput = "";
        System.out.println("Please, insert this informations to register yourself : ");
        createUser();
        do {
            menu();
            userInput = sc.nextLine();
            processUserInput(userInput);
        } while (!userInput.equals(QUIT) );
        sc.close();
    }

    private static void processUserInput(String userInput) {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +userInput.toString());
        System.out.println("********************");
        switch (userInput) {
            case "0" :
                {
                    repo.diplayProducts();
                    break;
                }
            case "1" :
                {
                    System.out.println("ID :");
                    String idProduct = sc.nextLine();
                    try {
                        System.out.println(repo.fetchProduct(idProduct));
                    } catch (ProductNotFoundException e) {
                        System.out.println("Exception : ");
                        System.out.println(e.getMessage());
                    }
                    break;
                }
            case "2" :
                {
                    System.out.println("Product ID :");
                    String idProduct = sc.nextLine();
                    System.out.println("Product name :");
                    String nameProduct = sc.nextLine();
                    System.out.println("Product price :");
                    String priceProduct = sc.nextLine();
                    System.out.println("Product expiration Date :");
                    String dateString = sc.nextLine();
                    try {
                        System.out.println(repo.addProduct(new Product(idProduct, nameProduct, priceProduct, dateString)));
                    } catch (ProductAlreadyExisteException e) {
                        System.out.println("Exception : ");
                        System.out.println(e.getMessage());
                    }
                    break;
                }
            case "3" :
                {
                    System.out.println("ID :");
                    String idProduct = sc.nextLine();
                    try {
                        System.out.println(repo.deleteProduct(idProduct));
                    } catch (ProductNotFoundException e) {
                        System.out.println("Exception : ");
                        System.out.println(e.getMessage());
                    }
                    break;
                }
            case "4" :
                {
                    System.out.println("Product ID :");
                    String idProduct = sc.nextLine();
                    System.out.println("Product name :");
                    String nameProduct = sc.nextLine();
                    System.out.println("Product price :");
                    String priceProduct = sc.nextLine();
                    System.out.println("Product expiration Date :");
                    String dateString = sc.nextLine();
                    try {
                        System.out.println(repo.updateProduct(new Product(idProduct, nameProduct, priceProduct, dateString)));
                    } catch (ProductNotFoundException | ProductAlreadyExisteException e) {
                        System.out.println("Exception : ");
                        System.out.println(e.getMessage());
                    }
                    break;
                }
            case "5" :
                {
                    initRepo();
                    break;
                }
            case QUIT :
                System.out.println("Bye...");
                return;
            default :
                System.err.println("Sorry, wrong input. Please try again.");
                return;
        }
    }

    private static void createUser() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        System.out.println("ID:");
        String idUser = sc.nextLine();
        System.out.println("name :");
        String nameUser = sc.nextLine();
        System.out.println("age :");
        String ageUser = sc.nextLine();
        System.out.println("email :");
        String emailUser = sc.nextLine();
        System.out.println("password :");
        String passwordUser = sc.nextLine();
        user.setID(idUser);
        user.setName(nameUser);
        user.setAge(ageUser);
        user.setEmail(emailUser);
        user.setPassword(passwordUser);
    }

    private static void menu() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        StringBuilder sb = new StringBuilder();
        sb.append("You can do the following actions :");
        sb.append('\n');
        String[] actions = new String[]{ "display products", "fetch a product by ID", "add a new Product", "delete a product by ID", "update a Product Info", "initialize with defaults" };
        for (int i = 0; i < actions.length; i++) {
            sb.append(i);
            sb.append(". ");
            sb.append(actions[i]);
            sb.append('\n');
        }
        sb.append(("\n" + QUIT) + ". To quit.");
        System.out.println(sb.toString());
    }

    private static void initRepo() {
        LOGGER.info(App.getCurrentUser().toString()+ ";" +"no parameters given");
        Product p1 = new Product("0", "fresh ground steak", "20.0", "11/11/26");
        Product p2 = new Product("1", "cheese omelette", "10.0", "11/11/27");
        Product p3 = new Product("2", "baguette", "1.0", "11/11/28");
        Product p4 = new Product("3", "croissant", "2.0", "11/11/29");
        Product p5 = new Product("4", "pain aux chocolat", "2.5", "11/12/01");
        try {
            repo.addProduct(p1);
            repo.addProduct(p2);
            repo.addProduct(p3);
            repo.addProduct(p4);
            repo.addProduct(p5);
        } catch (ProductAlreadyExisteException e) {
            System.out.println("Exception : ");
            System.out.println(e.getMessage());
        }
    }

    public static User getCurrentUser() {
        LOGGER.info("no parameters given");
        return user;
    }
}